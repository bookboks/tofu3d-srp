﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// [System.Serializable]
// public class MapButton
// {
//     [SerializeField] private Button mapButton;
//     [SerializeField] private string LevelToChange;
// }

public class MapScreenController : MonoBehaviour
{
    [SerializeField] private List<Button> mapOceanButton;
    [SerializeField] private string OceanMapName;
    [SerializeField] private List<Button> mapHillButton;
    [SerializeField] private string HillMapName;
    [SerializeField] private List<Button> mapSnowButton;
    [SerializeField] private string SnowMapName;
    [SerializeField] private GameObject HillLockScreen;
    [SerializeField] private GameObject SnowLockScreen;
    private bool isCheating;

    void Start()
    {
        isCheating = FindObjectOfType<CheatingController>().isCheatMap;
        checkAvaiableMap(mapOceanButton, OceanMapName);
        checkAvaiableMap(mapHillButton, HillMapName);
        checkAvaiableMap(mapSnowButton, SnowMapName);
        checkLockScreen();
    }

    void checkAvaiableMap(List<Button> Maps, string typeMapName)
    {
        for (int i = 0; i < Maps.Count; i++)
        {
            string levelToChange = typeMapName + " " + (i + 1);
            Maps[i].onClick.AddListener(() => changeToLevel(levelToChange));
            if (PlayerPrefs.GetInt(levelToChange) == 1 || levelToChange.Equals("OLevel 1") || isCheating)
            {
                Maps[i].interactable = true;
            }
            else
            {
                Maps[i].interactable = false;
            }
        }
    }

    void checkLockScreen()
    {
        if (PlayerPrefs.GetInt("HLevel 1") == 1 || isCheating)
            HillLockScreen.SetActive(false);
        if (PlayerPrefs.GetInt("SLevel 1") == 1 || isCheating)
            SnowLockScreen.SetActive(false);
    }

    public void changeToLevel(string levelToChange)
    {
        SceneManager.LoadScene(levelToChange);
    }
}


