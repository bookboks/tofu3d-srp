﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    public GameObject startObject;

    [SerializeField] private int minDistanceFromPlayer;
    [SerializeField] private int maxTerrainCount;
    [SerializeField] private List<TerrainData> terrainDatas = new List<TerrainData>();
    [SerializeField] private Transform terrainHolder;

    [SerializeField] private Vector3 currentPosition = new Vector3(0, 0, 0);
    [SerializeField] private GameObject Coin1Prefab;
    [SerializeField] private GameObject Coin2Prefab;
    [SerializeField] private GameObject Coin4Prefab;
    [SerializeField] private GameObject Coin5Prefab;
    [SerializeField] private GameObject Coin9Prefab;
    private List<GameObject> currentTerrains = new List<GameObject>();
    private List<GameObject> currentCoins = new List<GameObject>();
    private PlayerController thePlayer;
    private int beforeTerrain;
    private int directionCount;
    private bool isDirectionFont;

    void Start()
    {
        isDirectionFont = true;
        directionCount = 0;
        ResetTerrain();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            ResetTerrain();
        }
    }

    public void SpawnTerrain(Vector3 playerPos)
    {
        while (currentPosition.z - playerPos.z < minDistanceFromPlayer)
        {
            int whichTerrain = Random.Range(0, terrainDatas.Count);
            int terrainInSuccession = Random.Range(1, terrainDatas[whichTerrain].maxInSuccession);
            int terrainRange = Random.Range(1, 3);
            for (int i = 0; i < terrainInSuccession; i++)
            {
                if (isDirectionFont)
                {
                    GameObject terrain = Instantiate(terrainDatas[whichTerrain].terrain, currentPosition, Quaternion.identity, terrainHolder);
                    currentTerrains.Add(terrain);
                    currentPosition.z += terrainRange;
                }
                // else
                // {
                //     GameObject terrain = Instantiate(terrainDatas[whichTerrain].terrain, currentPosition, Quaternion.identity, terrainHolder);
                //     currentTerrains.Add(terrain);
                //     currentPosition.x -= terrainRange;
                // }
                // directionCount++;
                // if (directionCount > 3)
                // {
                //     isDirectionFont = !isDirectionFont;
                //     directionCount = 0;
                // }

                if (thePlayer = FindObjectOfType<PlayerController>())
                {
                    int coinRandom = Random.Range(0, 2);
                    if (coinRandom > 0)
                        if (currentPosition.z > 500)
                        {
                            GameObject CoinObject = Instantiate(Coin9Prefab, new Vector3(currentPosition.x, currentPosition.y + 0.95f, currentPosition.z), Quaternion.identity, terrainHolder);
                            currentCoins.Add(CoinObject);
                        }
                        else if (currentPosition.z > 200)
                        {
                            GameObject CoinObject = Instantiate(Coin5Prefab, new Vector3(currentPosition.x, currentPosition.y + 0.95f, currentPosition.z), Quaternion.identity, terrainHolder);
                            currentCoins.Add(CoinObject);
                        }
                        else if (currentPosition.z > 100)
                        {
                            GameObject CoinObject = Instantiate(Coin4Prefab, new Vector3(currentPosition.x, currentPosition.y + 0.95f, currentPosition.z), Quaternion.identity, terrainHolder);
                            currentCoins.Add(CoinObject);
                        }
                        else if (currentPosition.z > 50)
                        {
                            GameObject CoinObject = Instantiate(Coin2Prefab, new Vector3(currentPosition.x, currentPosition.y + 0.95f, currentPosition.z), Quaternion.identity, terrainHolder);
                            currentCoins.Add(CoinObject);
                        }
                        else if (currentPosition.z > 10)
                        {
                            GameObject CoinObject = Instantiate(Coin1Prefab, new Vector3(currentPosition.x, currentPosition.y + 0.95f, currentPosition.z), Quaternion.identity, terrainHolder);
                            currentCoins.Add(CoinObject);
                        }
                }

                if (currentTerrains.Count > maxTerrainCount)
                {
                    Destroy(currentTerrains[0]);
                    currentTerrains.RemoveAt(0);
                }
                if (currentCoins.Count > maxTerrainCount)
                {
                    Destroy(currentCoins[0]);
                    currentCoins.RemoveAt(0);
                }
            }
        }
    }

    public void ResetTerrain()
    {
        for (int i = 0; i < currentTerrains.Count; i++)
        {
            Destroy(currentTerrains[i]);
        }
        for (int i = 0; i < currentCoins.Count; i++)
        {
            Destroy(currentCoins[i]);
        }
        currentTerrains.Clear();
        currentCoins.Clear();
        if (startObject)
        {
            currentPosition = startObject.transform.position;
            currentPosition.z++;
        }
        for (int i = 0; i < maxTerrainCount; i++)
            SpawnTerrain(new Vector3(0, 0, 0));
    }
}
