﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateUICoin : MonoBehaviour
{
    public GameObject StartTarget;
    public GameObject StartTargetPlus;
    public GameObject MoveCoin;
    public GameObject SetCanvas;

    public void CreateMoveCoin()
    {
        GameObject NewMoveCoin = Instantiate(MoveCoin, StartTarget.transform.position, StartTarget.transform.rotation);
        NewMoveCoin.transform.SetParent(SetCanvas.transform, false);
        NewMoveCoin.transform.position = StartTarget.transform.position;
    }

    public void CreateMoveCoinPlus()
    {
        GameObject NewMoveCoin = Instantiate(MoveCoin, StartTargetPlus.transform.position, StartTargetPlus.transform.rotation);
        NewMoveCoin.transform.SetParent(SetCanvas.transform, false);
        NewMoveCoin.transform.position = StartTargetPlus.transform.position;
    }
}
