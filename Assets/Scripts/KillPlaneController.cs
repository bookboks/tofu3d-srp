﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlaneController : MonoBehaviour
{
    private Vector3 startPosition;
    private Vector3 currentPosition;

    void Start()
    {
        startPosition = transform.position;
        currentPosition = transform.position;
    }

    public void moveKillPlane(Vector3 PlayerPos)
    {
        if (currentPosition.z - PlayerPos.z < 1.0f)
        {
            currentPosition = new Vector3(transform.position.x, transform.position.y, PlayerPos.z + 10);
            transform.position = currentPosition;
        }
    }

    public void ResetKillPlane()
    {
        transform.position = startPosition;
        currentPosition = startPosition;
    }
}
