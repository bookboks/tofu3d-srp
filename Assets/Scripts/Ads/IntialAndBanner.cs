﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Monetization;

public class IntialAndBanner : MonoBehaviour
{

#if UNITY_IOS
    private const string gameId = "3312038";
#elif UNITY_ANDROID
    private const string gameId = "3312039";
#endif

    public string placementId = "banner";
    public bool testMode = false;
    [SerializeField] private bool isShowBanner;

    void Start()
    {
        isShowBanner = true;
        if (PlayerPrefs.GetInt("isNoAds") == 1)
        {
            HideBanner();
            isShowBanner = false;
        }

        if (isShowBanner)
        {
            //Advertisement.Initialize(gameId, testMode);
            StartCoroutine(ShowBannerWhenReady());
        }

        if (Monetization.isSupported)
        {
            Monetization.Initialize(gameId, testMode);
        }

    }

    IEnumerator ShowBannerWhenReady()
    {
        while (!Advertisement.IsReady(placementId))
        {
            yield return new WaitForSeconds(0.5f);
        }
        Advertisement.Banner.Show(placementId);
    }

    public void HideBanner()
    {
        Advertisement.Banner.Hide();
    }
}