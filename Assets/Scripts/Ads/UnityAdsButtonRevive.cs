﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Monetization;

[RequireComponent(typeof(Button))]
public class UnityAdsButtonRevive : MonoBehaviour
{

    public string placementId = "rewardedVideo";
    private Button adButton;

#if UNITY_IOS
    private string gameId = "3224922";
#elif UNITY_ANDROID
    private string gameId = "3224923";
#elif UNITY_STANDALONE_WIN
    private string gameId = "3224923";
#endif

    void Start()
    {
        adButton = GetComponent<Button>();
        if (adButton)
        {
            adButton.onClick.AddListener(ShowAd);
        }

        if (Monetization.isSupported)
        {
            Monetization.Initialize(gameId, true);
        }
    }

    void Update()
    {
        if (adButton)
        {
            adButton.interactable = Monetization.IsReady(placementId);
        }
    }

    public void ShowAd()
    {
        ShowAdCallbacks options = new ShowAdCallbacks();
        options.finishCallback = HandleShowResult;
        ShowAdPlacementContent ad = Monetization.GetPlacementContent(placementId) as ShowAdPlacementContent;
        ad.Show(options);
    }

    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            // Reward the player
            PlayerController thePlayer = FindObjectOfType<PlayerController>();
            thePlayer.Revive();
        }
        else if (result == ShowResult.Skipped)
        {
            PlayerController thePlayer = FindObjectOfType<PlayerController>();
            thePlayer.StartCoroutineRespawn();
        }
        else if (result == ShowResult.Failed)
        {
            PlayerController thePlayer = FindObjectOfType<PlayerController>();
            thePlayer.StartCoroutineRespawn();
        }
    }
}