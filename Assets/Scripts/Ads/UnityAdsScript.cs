﻿using UnityEngine.Monetization;
using UnityEngine;
public class UnityAdsScript : MonoBehaviour
{

#if UNITY_IOS
    private string gameId = "3224922";
#elif UNITY_ANDROID
    private string gameId = "3224923";
#elif UNITY_STANDALONE_WIN
    private string gameId = "3224923";
#endif
    bool testMode = true;

    void Start()
    {
        Monetization.Initialize(gameId, testMode);
    }
}