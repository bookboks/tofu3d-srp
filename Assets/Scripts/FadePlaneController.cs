﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadePlaneController : MonoBehaviour
{
    private float FadePlaneTime;
    private float FadePlaneCounter;
    void Start()
    {
        FadePlaneTime = 1.0f;
        FadePlaneCounter = FadePlaneTime;
    }

    void Update()
    {
        if (FadePlaneCounter >= 0)
        {
            FadePlaneCounter -= Time.deltaTime;
        }
        if (FadePlaneCounter < 0)
        {
            Destroy(gameObject);
        }
    }
}
