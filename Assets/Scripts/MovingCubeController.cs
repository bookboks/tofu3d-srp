﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCubeController : MonoBehaviour
{
    [SerializeField] private GameObject startObject;
    [SerializeField] private GameObject stopObject;
    private Vector3 startPosition;
    private Vector3 stopPosition;
    [SerializeField] private float speed;
    [SerializeField] private bool isUpDown;
    [SerializeField] private bool invertUpDown;
    [SerializeField] private float upDownDistance;
    private bool moveStart;
    private float movingCounter;
    private float movingTime;

    void Start()
    {

        moveStart = true;
        movingTime = 2.0f;

        if (!isUpDown)
        {
            startPosition = startObject.transform.position;
            stopPosition = stopObject.transform.position;
        }
        else if (isUpDown)
        {
            startPosition = new Vector3(transform.position.x, transform.position.y + upDownDistance, transform.position.z);
            stopPosition = new Vector3(transform.position.x, transform.position.y - upDownDistance, transform.position.z);
            if (invertUpDown)
            {
                startPosition = new Vector3(transform.position.x, transform.position.y - upDownDistance, transform.position.z);
                stopPosition = new Vector3(transform.position.x, transform.position.y + upDownDistance, transform.position.z);
            }
        }
    }

    void FixedUpdate()
    {
        float step = speed * Time.deltaTime;

        if (!isUpDown)
            if (moveStart)
            {
                transform.position = Vector3.MoveTowards(transform.position, startObject.transform.position, step);
                if (transform.position == startObject.transform.position)
                    moveStart = false;
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, stopObject.transform.position, step);
                if (transform.position == stopObject.transform.position)
                    moveStart = true;
            }
        else if (isUpDown)
        {
            if (moveStart)
            {
                transform.position = Vector3.MoveTowards(transform.position, startPosition, step);
                if (transform.position == startPosition)
                    moveStart = false;
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, stopPosition, step);
                if (transform.position == stopPosition)
                    moveStart = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !isUpDown)
        {
            other.transform.parent = transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && !isUpDown)
        {
            other.transform.parent = null;
        }
    }
}
