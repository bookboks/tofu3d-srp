﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    //old camera y-20 xPlus2
    //new camera y-11 xPlus1.1
    public GameObject target;
    public float followAhead;

    public Vector3 targetPosition;
    private Vector3 startTargetPosition;
    public float XPlus;
    public float YPlus;
    public float ZPlus;

    public float smoothing;
    public float smoothingY;

    public bool followTarget;
    public bool fixGround;
    public bool fixedX;

    private float limitedYdown;
    public bool isStartGame = false;
    [HideInInspector]

    public bool Lerp;
    [SerializeField] private Vector3 shopPosition;
    [SerializeField] private Vector3 startPosition;

    void Start()
    {
        startPosition = transform.position;
        startTargetPosition = targetPosition;
        limitedYdown = transform.position.y;
    }

    void FixedUpdate()
    {
        if (followTarget && target && isStartGame)
        {
            if (fixedX)
                targetPosition = new Vector3(transform.position.x, transform.position.y, target.transform.position.z + ZPlus);
            else
                targetPosition = new Vector3(target.transform.position.x - startTargetPosition.x + startPosition.x + XPlus, transform.position.y + YPlus, target.transform.position.z - startTargetPosition.z + startPosition.z + ZPlus);

            if (Lerp)
                transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing * Time.deltaTime);
            else
                transform.position = targetPosition;
        }
    }
}
