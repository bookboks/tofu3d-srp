﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    private PlayerController thePlayer;
    private ShopController theShop;
    private Rigidbody myRigidbody;

    static string currentCharacter = "Tofu";
    public GameObject Tofu;
    public GameObject Fotu;
    public GameObject Metal;
    public GameObject Cotton;
    public GameObject Paper;
    public GameObject Rubber;

    [Header("Screen2")]
    public GameObject Rocket;
    public GameObject Ice;
    public GameObject SandPaper;
    public GameObject Moon;

    public AudioSource changeCharacterSound;
    private bool isStarting;

    void Start()
    {
        thePlayer = FindObjectOfType<PlayerController>();
        isStarting = true;

        ChangeCharacter(currentCharacter);
        isStarting = false;
    }

    public void ChangeCharacter(string name)
    {
        ChangeCharacterOnly(name);

        theShop = FindObjectOfType<ShopController>();
        if (theShop)
            theShop.UpdateShop();
    }

    public void ChangeCharacterOnly(string name)
    {
        if (!isStarting)
            changeCharacterSound.Play();

        myRigidbody = thePlayer.GetComponent<Rigidbody>();
        myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, thePlayer.turnFaceJumpSpeed, myRigidbody.velocity.z);

        Tofu.SetActive(false);
        Fotu.SetActive(false);
        Metal.SetActive(false);
        Cotton.SetActive(false);
        Paper.SetActive(false);
        Rubber.SetActive(false);
        Rocket.SetActive(false);
        Ice.SetActive(false);
        SandPaper.SetActive(false);
        Moon.SetActive(false);

        if (name.Equals("Tofu"))
            Tofu.SetActive(true);
        else if (name.Equals("Fotu"))
            Fotu.SetActive(true);
        else if (name.Equals("Metal"))
            Metal.SetActive(true);
        else if (name.Equals("Cotton"))
            Cotton.SetActive(true);
        else if (name.Equals("Paper"))
            Paper.SetActive(true);
        else if (name.Equals("Rubber"))
            Rubber.SetActive(true);
        else if (name.Equals("Rocket"))
            Rocket.SetActive(true);
        else if (name.Equals("Ice"))
            Ice.SetActive(true);
        else if (name.Equals("SandPaper"))
            SandPaper.SetActive(true);
        else if (name.Equals("Moon"))
            Moon.SetActive(true);

        currentCharacter = name;
    }

    public string GetCharacterName()
    {
        return currentCharacter;
    }
}
