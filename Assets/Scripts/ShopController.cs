using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopController : MonoBehaviour
{
    static string currentSelectCharacter = "Tofu";
    private LevelManager theLevelManager;
    public CharacterController theCharacter;

    public GameObject TofuGameObject;
    private Outline TofuOutline;

    private int isFotuSold;
    private string isFotuSoldString;
    public GameObject FotuGameObject;
    private Outline FotuOutline;
    private Image FotuImage;
    public Sprite FotuSprite;

    private int isMetalSold;
    private string isMetalSoldString;
    public GameObject MetalGameObject;
    private Outline MetalOutline;
    private Image MetalImage;
    public Sprite MetalSprite;

    private int isCottonSold;
    private string isCottonSoldString;
    public GameObject CottonGameObject;
    private Outline CottonOutline;
    private Image CottonImage;
    public Sprite CottonSprite;

    private int isPaperSold;
    private string isPaperSoldString;
    public GameObject PaperGameObject;
    private Outline PaperOutline;
    private Image PaperImage;
    public Sprite PaperSprite;

    private int isRubberSold;
    private string isRubberSoldString;
    public GameObject RubberGameObject;
    private Outline RubberOutline;
    private Image RubberImage;
    public Sprite RubberSprite;

    private int isRocketSold;
    private string isRocketSoldString;
    public GameObject RocketGameObject;
    private Outline RocketOutline;
    private Image RocketImage;
    public Sprite RocketSprite;

    private int isIceSold;
    private string isIceSoldString;
    public GameObject IceGameObject;
    private Outline IceOutline;
    private Image IceImage;
    public Sprite IceSprite;

    private int isSandPaperSold;
    private string isSandPaperSoldString;
    public GameObject SandPaperGameObject;
    private Outline SandPaperOutline;
    private Image SandPaperImage;
    public Sprite SandPaperSprite;

    private int isMoonSold;
    private string isMoonSoldString;
    public GameObject MoonGameObject;
    private Outline MoonOutline;
    private Image MoonImage;
    public Sprite MoonSprite;

    public Sprite BlackSprite;
    public Button BuyButton;
    private int amount;
    private ArrayList unSoldList = new ArrayList();

    void Start()
    {
        PlayerPrefs.SetString("PreviousScene", SceneManager.GetActiveScene().name);
        theLevelManager = FindObjectOfType<LevelManager>();

        TofuOutline = TofuGameObject.GetComponent<Outline>();

        FotuOutline = FotuGameObject.GetComponent<Outline>();
        FotuImage = FotuGameObject.GetComponent<Image>();

        MetalOutline = MetalGameObject.GetComponent<Outline>();
        MetalImage = MetalGameObject.GetComponent<Image>();

        CottonOutline = CottonGameObject.GetComponent<Outline>();
        CottonImage = CottonGameObject.GetComponent<Image>();

        PaperOutline = PaperGameObject.GetComponent<Outline>();
        PaperImage = PaperGameObject.GetComponent<Image>();

        RubberOutline = RubberGameObject.GetComponent<Outline>();
        RubberImage = RubberGameObject.GetComponent<Image>();

        RocketOutline = RocketGameObject.GetComponent<Outline>();
        RocketImage = RocketGameObject.GetComponent<Image>();

        IceOutline = IceGameObject.GetComponent<Outline>();
        IceImage = IceGameObject.GetComponent<Image>();

        SandPaperOutline = SandPaperGameObject.GetComponent<Outline>();
        SandPaperImage = SandPaperGameObject.GetComponent<Image>();

        MoonOutline = MoonGameObject.GetComponent<Outline>();
        MoonImage = MoonGameObject.GetComponent<Image>();

        if (FindObjectOfType<CheatingController>().isCheatCharactor)
            cheatCharacter();

        UpdateShop();
        if (theCharacter)
            UpdateSelectButton(theCharacter.GetCharacterName());
    }

    private void cheatCharacter()
    {
        PlayerPrefs.SetInt("IsFotuSold", 1);
        PlayerPrefs.SetInt("IsMetalSold", 1);
        PlayerPrefs.SetInt("IsCottonSold", 1);
        PlayerPrefs.SetInt("IsPaperSold", 1);
        PlayerPrefs.SetInt("IsRubberSold", 1);
        PlayerPrefs.SetInt("IsRocketSold", 1);
        PlayerPrefs.SetInt("IsIceSold", 1);
        PlayerPrefs.SetInt("IsSandPaperSold", 1);
        PlayerPrefs.SetInt("IsMoonSold", 1);
    }

    public void OnClickRandom()
    {
        SceneManager.LoadScene("Gacha");
    }

    public void UpdateShop()
    {
        amount = PlayerPrefs.GetInt("Coin");
        CheckBuyButton();
        CheckFotu();
        CheckMetal();
        CheckCotton();
        CheckPaper();
        CheckRubber();
        CheckRocket();
        CheckIce();
        CheckSandPaper();
        CheckMoon();
    }

    public void CheckBuyButton()
    {
        unSoldList.Clear();
        if (PlayerPrefs.GetInt("IsFotuSold") == 0)
            unSoldList.Add("Fotu");
        if (PlayerPrefs.GetInt("IsMetalSold") == 0)
            unSoldList.Add("Metal");
        if (PlayerPrefs.GetInt("IsCottonSold") == 0)
            unSoldList.Add("Cotton");
        if (PlayerPrefs.GetInt("IsPaperSold") == 0)
            unSoldList.Add("Paper");
        if (PlayerPrefs.GetInt("IsRubberSold") == 0)
            unSoldList.Add("Rubber");
        if (PlayerPrefs.GetInt("IsRocketSold") == 0)
            unSoldList.Add("Rocket");
        if (PlayerPrefs.GetInt("IsIceSold") == 0)
            unSoldList.Add("Ice");
        if (PlayerPrefs.GetInt("IsSandPaperSold") == 0)
            unSoldList.Add("SandPaper");
        if (PlayerPrefs.GetInt("IsMoonSold") == 0)
            unSoldList.Add("Moon");

        if (amount >= theLevelManager.randomGachaPrice && unSoldList.Count > 0)
            BuyButton.interactable = true;
        else
            BuyButton.interactable = false;
    }

    public void UpdateSelectButton(string characterName)
    {
        TofuOutline.enabled = false;
        FotuOutline.enabled = false;
        MetalOutline.enabled = false;
        CottonOutline.enabled = false;
        PaperOutline.enabled = false;
        RubberOutline.enabled = false;
        RocketOutline.enabled = false;
        IceOutline.enabled = false;
        SandPaperOutline.enabled = false;
        MoonOutline.enabled = false;

        if (characterName.Equals("Tofu"))
        {
            currentSelectCharacter = "Tofu";
            TofuOutline.enabled = true;
            theCharacter.ChangeCharacterOnly("Tofu");
        }
        else if (characterName.Equals("Fotu"))
        {
            currentSelectCharacter = "Fotu";
            FotuOutline.enabled = true;
            isFotuSold = PlayerPrefs.GetInt("IsFotuSold");
            if (isFotuSold == 1)
                theCharacter.ChangeCharacterOnly("Fotu");
        }
        else if (characterName.Equals("Metal"))
        {
            currentSelectCharacter = "Metal";
            MetalOutline.enabled = true;
            isMetalSold = PlayerPrefs.GetInt("IsMetalSold");
            if (isMetalSold == 1)
                theCharacter.ChangeCharacterOnly("Metal");
        }
        else if (characterName.Equals("Cotton"))
        {
            currentSelectCharacter = "Cotton";
            CottonOutline.enabled = true;
            isCottonSold = PlayerPrefs.GetInt("IsCottonSold");
            if (isCottonSold == 1)
                theCharacter.ChangeCharacterOnly("Cotton");
        }
        else if (characterName.Equals("Paper"))
        {
            currentSelectCharacter = "Paper";
            PaperOutline.enabled = true;
            isPaperSold = PlayerPrefs.GetInt("IsPaperSold");
            if (isPaperSold == 1)
                theCharacter.ChangeCharacterOnly("Paper");
        }
        else if (characterName.Equals("Rubber"))
        {
            currentSelectCharacter = "Rubber";
            RubberOutline.enabled = true;
            isRubberSold = PlayerPrefs.GetInt("IsRubberSold");
            if (isRubberSold == 1)
                theCharacter.ChangeCharacterOnly("Rubber");
        }
        else if (characterName.Equals("Rocket"))
        {
            currentSelectCharacter = "Rocket";
            RocketOutline.enabled = true;
            isRocketSold = PlayerPrefs.GetInt("IsRocketSold");
            if (isRocketSold == 1)
                theCharacter.ChangeCharacterOnly("Rocket");
        }
        else if (characterName.Equals("Ice"))
        {
            currentSelectCharacter = "Ice";
            IceOutline.enabled = true;
            isIceSold = PlayerPrefs.GetInt("IsIceSold");
            if (isIceSold == 1)
                theCharacter.ChangeCharacterOnly("Ice");
        }
        else if (characterName.Equals("SandPaper"))
        {
            currentSelectCharacter = "SandPaper";
            SandPaperOutline.enabled = true;
            isSandPaperSold = PlayerPrefs.GetInt("IsSandPaperSold");
            if (isSandPaperSold == 1)
                theCharacter.ChangeCharacterOnly("SandPaper");
        }
        else if (characterName.Equals("Moon"))
        {
            currentSelectCharacter = "Moon";
            MoonOutline.enabled = true;
            isMoonSold = PlayerPrefs.GetInt("IsMoonSold");
            if (isMoonSold == 1)
                theCharacter.ChangeCharacterOnly("Moon");
        }
    }

    #region Screen1
    public void CheckFotu()
    {
        isFotuSold = PlayerPrefs.GetInt("IsFotuSold");
        if (isFotuSold == 0)
        {
            FotuImage.sprite = BlackSprite;
        }
        else if (isFotuSold == 1)
        {
            FotuImage.sprite = FotuSprite;
        }
    }
    public void CheckMetal()
    {
        isMetalSold = PlayerPrefs.GetInt("IsMetalSold");
        if (isMetalSold == 0)
        {
            MetalImage.sprite = BlackSprite;
        }
        else if (isMetalSold == 1)
        {
            MetalImage.sprite = MetalSprite;
        }
    }

    public void CheckCotton()
    {
        isCottonSold = PlayerPrefs.GetInt("IsCottonSold");
        if (isCottonSold == 0)
        {
            CottonImage.sprite = BlackSprite;
        }
        else if (isCottonSold == 1)
        {
            CottonImage.sprite = CottonSprite;
        }
    }

    public void CheckPaper()
    {
        isPaperSold = PlayerPrefs.GetInt("IsPaperSold");
        if (isPaperSold == 0)
        {
            PaperImage.sprite = BlackSprite;
        }
        else if (isPaperSold == 1)
        {
            PaperImage.sprite = PaperSprite;
        }
    }

    public void CheckRubber()
    {
        isRubberSold = PlayerPrefs.GetInt("IsRubberSold");
        if (isRubberSold == 0)
        {
            RubberImage.sprite = BlackSprite;
        }
        else if (isRubberSold == 1)
        {
            RubberImage.sprite = RubberSprite;
        }
    }
    #endregion

    #region Screen2
    public void CheckRocket()
    {
        isRocketSold = PlayerPrefs.GetInt("IsRocketSold");
        if (isRocketSold == 0)
        {
            RocketImage.sprite = BlackSprite;
        }
        else if (isRocketSold == 1)
        {
            RocketImage.sprite = RocketSprite;
        }
    }

    public void CheckIce()
    {
        isIceSold = PlayerPrefs.GetInt("IsIceSold");
        if (isIceSold == 0)
        {
            IceImage.sprite = BlackSprite;
        }
        else if (isIceSold == 1)
        {
            IceImage.sprite = IceSprite;
        }
    }

    public void CheckSandPaper()
    {
        isSandPaperSold = PlayerPrefs.GetInt("IsSandPaperSold");
        if (isSandPaperSold == 0)
        {
            SandPaperImage.sprite = BlackSprite;
        }
        else if (isSandPaperSold == 1)
        {
            SandPaperImage.sprite = SandPaperSprite;
        }
    }

    public void CheckMoon()
    {
        isMoonSold = PlayerPrefs.GetInt("IsMoonSold");
        if (isMoonSold == 0)
        {
            MoonImage.sprite = BlackSprite;
        }
        else if (isMoonSold == 1)
        {
            MoonImage.sprite = MoonSprite;
        }
    }
    #endregion
}
