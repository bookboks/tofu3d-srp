﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GachaponController : MonoBehaviour
{
    [SerializeField]
    private Animator theAnime;
    [SerializeField]
    private Button DoneButton;

    [SerializeField] private GameObject Mystery;
    [SerializeField] private GameObject Tofu;
    [SerializeField] private GameObject Fotu;
    [SerializeField] private GameObject Metal;
    [SerializeField] private GameObject Cotton;
    [SerializeField] private GameObject Paper;
    [SerializeField] private GameObject Rubber;
    [SerializeField] private GameObject Rocket;
    [SerializeField] private GameObject Ice;
    [SerializeField] private GameObject SandPaper;
    [SerializeField] private GameObject Moon;

    [SerializeField] private Animator NameTextAnime;
    [SerializeField] private Text CharacterText;
    [SerializeField] private GameObject CelebrationObject;
    [SerializeField] private GameObject CelebrationSpinObject;

    [SerializeField] private AudioSource openGachaSound;
    [SerializeField] private AudioSource clickButtonSound;


    private ArrayList unSoldList = new ArrayList();
    void Start()
    {
        openGachaSound.Play();
        if (PlayerPrefs.GetInt("IsFotuSold") == 0)
            unSoldList.Add("Fotu");
        if (PlayerPrefs.GetInt("IsMetalSold") == 0)
            unSoldList.Add("Metal");
        if (PlayerPrefs.GetInt("IsCottonSold") == 0)
            unSoldList.Add("Cotton");
        if (PlayerPrefs.GetInt("IsPaperSold") == 0)
            unSoldList.Add("Paper");
        if (PlayerPrefs.GetInt("IsRubberSold") == 0)
            unSoldList.Add("Rubber");
        if (PlayerPrefs.GetInt("IsRocketSold") == 0)
            unSoldList.Add("Rocket");
        if (PlayerPrefs.GetInt("IsIceSold") == 0)
            unSoldList.Add("Ice");
        if (PlayerPrefs.GetInt("IsSandPaperSold") == 0)
            unSoldList.Add("SandPaper");
        if (PlayerPrefs.GetInt("IsMoonSold") == 0)
            unSoldList.Add("Moon");

        DoneButton.interactable = false;
        StartCoroutine(OpenGacha());
    }

    void Update()
    {
        if (Input.anyKey)
            if (Input.GetButtonDown("Reset"))
            {
                PlayerPrefs.DeleteAll();
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
    }

    IEnumerator OpenGacha()
    {
        theAnime.SetBool("isOpenGacha", true);
        yield return new WaitForSeconds(3.85f);
        int randomNumber = Random.Range(0, unSoldList.Count);
        UnlockCharacter((string)unSoldList[randomNumber]);
        PlayCharacterTextAnime((string)unSoldList[randomNumber]);
        Mystery.SetActive(false);
        CelebrationObject.SetActive(true);
        CelebrationSpinObject.SetActive(true);
        yield return new WaitForSeconds(0.6f);
        DoneButton.interactable = true;
    }

    public void PlayCharacterTextAnime(string name)
    {
        CharacterText.text = name;
        NameTextAnime.SetBool("isPopup", true);
    }

    public void UnlockCharacter(string name)
    {
        if (name.Equals("Fotu"))
        {
            Fotu.SetActive(true);
            PlayerPrefs.SetInt("IsFotuSold", 1);
        }
        else if (name.Equals("Metal"))
        {
            Metal.SetActive(true);
            PlayerPrefs.SetInt("IsMetalSold", 1);
        }
        else if (name.Equals("Cotton"))
        {
            Cotton.SetActive(true);
            PlayerPrefs.SetInt("IsCottonSold", 1);
        }
        else if (name.Equals("Paper"))
        {
            Paper.SetActive(true);
            PlayerPrefs.SetInt("IsPaperSold", 1);
        }
        else if (name.Equals("Rubber"))
        {
            Rubber.SetActive(true);
            PlayerPrefs.SetInt("IsRubberSold", 1);
        }
        else if (name.Equals("Rocket"))
        {
            Rocket.SetActive(true);
            PlayerPrefs.SetInt("IsRocketSold", 1);
        }
        else if (name.Equals("Ice"))
        {
            Ice.SetActive(true);
            PlayerPrefs.SetInt("IsIceSold", 1);
        }
        else if (name.Equals("SandPaper"))
        {
            SandPaper.SetActive(true);
            PlayerPrefs.SetInt("IsSandPaperSold", 1);
        }
        else if (name.Equals("Moon"))
        {
            Moon.SetActive(true);
            PlayerPrefs.SetInt("IsMoonSold", 1);
        }
        int amount = PlayerPrefs.GetInt("Coin");
        PlayerPrefs.SetInt("Coin", amount - 200);
    }

    public void OnClickDone()
    {
        clickButtonSound.Play();
        SceneManager.LoadScene(PlayerPrefs.GetString("PreviousScene"));
    }
}
