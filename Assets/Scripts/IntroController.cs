﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour
{
    void Start()
    {
#if UNITY_STANDALONE_WIN
        Application.targetFrameRate = 500;
#else
        Application.targetFrameRate = 60;
#endif
        StartCoroutine(PlayIntro());
    }

    private IEnumerator PlayIntro()
    {
        yield return new WaitForSeconds(3f);
        string SceneToChange = PlayerPrefs.GetString("LastSceneName");
        Debug.Log(SceneToChange);
        Debug.Log(SceneManager.GetSceneByName(SceneToChange).IsValid());
        if (!SceneToChange.Equals(""))
        {
            SceneManager.LoadScene(SceneToChange);
        }
        else
        {
            SceneManager.LoadScene("OLevel 1");
        }
    }
}
