﻿using UnityEngine;
using UnityEngine.iOS;
using UnityEngine.SceneManagement;

public class AppRating : MonoBehaviour
{
    private const string iOSRatingURL = "https://apps.apple.com/us/app/mario-kart-tour/id1293634699";
    private const string AndroidRatingURL = "https://play.google.com/store/apps/details?id=com.nintendo.zaka";
    //Application.OpenURL("market://details?id=" + Application.productName);
    private bool isRated;

    void Start()
    {
        isRated = checkIsRatting();

        string currentMap = SceneManager.GetActiveScene().name;
        // string currentMapName = currentMap.Split(' ')[0];
        // string currentMapNumber = currentMap.Split(' ')[1];
        if (currentMap.Equals("OLevel 10") && !isRated)
        {
            Review();
        }
    }

    private bool checkIsRatting()
    {
        if (PlayerPrefs.GetInt("isRated") == 1)
            return true;
        else
            return false;
    }

    public void Review()
    {
#if UNITY_IOS
        Device.RequestStoreReview();
        // Application.OpenURL(iOSRatingURL);
#elif UNITY_ANDROID
        Application.OpenURL(AndroidRatingURL);
#else
        Application.OpenURL(AndroidRatingURL);
#endif
        PlayerPrefs.SetInt("isRated", 1);
    }
}
