﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DanielLochner.Assets.SimpleScrollSnap;

public class LevelManager : MonoBehaviour
{
    static int FinishTextedLevelCount = 0;
    static string currentSceneName;

    [Header("Under Text")]
    public int randomGachaPrice;
    public float fadeOutTime;


    private PlayerController thePlayer;
    private CameraController theCamera;
    private ShopController theShop;
    private CreateUICoin theUICoin;
    public bool isStartGame;



    [Header("Under Text")]
    public GameObject StartTextObject;
    public Text StartText;
    public GameObject gameOverScreen;
    public Text Over;
    public GameObject goalScreen;
    public Text FinishText;
    public Text DeathText;

    [Header("Count Text")]
    public Text jumpCounter;
    public Text jumpGroundCount;
    public Text coinCounter;
    public GameObject theShopScreen;
    public Text levelCounter;

    [Header("Button UI")]
    public Image shopButton;
    public Text shopButtonText;
    public GameObject shopButtonObject;
    public GameObject mapButtonObject;
    public GameObject noAdsButtonObject;


    public GameObject PauseScreen;
    public Text TopDistanceText;
    public GameObject theMapScreen;
    private bool isMapScreenOpen;

    public Text X1ButtonText;
    public GameObject X1ButtonObject;
    public Text X2ButtonText;
    public GameObject X2ButtonObject;
    public Text X3ButtonText;
    public GameObject X3ButtonObject;
    public Text X4ButtonText;
    public GameObject X4ButtonObject;
    public GameObject stageCoinObject1;
    public Text stageCoin1;
    public GameObject NoThanksObject;
    public Text NoThanksText;
    public Text ReviveText;
    public GameObject ReviveButtonObject;

    [Header("ObjectToReset")]
    public ResetOnRespawn[] objectsToReset;
    public GameObject[] objectsToResetParent;

    [Header("Camera")]
    [SerializeField] private GameObject Camera1;
    [SerializeField] private GameObject Camera2;
    [SerializeField] private GameObject Camera3;
    [SerializeField] private GameObject Camera4;
    private int cameraCount = 1;

    private string levelToChange;
    private int moveCoinTochangeSceneCounter;
    private bool isCoinCount;
    private bool isFPS30;
    public bool isAdsClicked;
    public bool isShopOpen;

    [Header("Animator")]
    public Animator theMapOpenAnimator;
    public Animator Screen1Animator;
    private int shopScreenPage;
    private int mapScreenPage;
    public Animator AnimeAddingCoinUI;
    public Animator AnimatorShopButton;
    public Animator AnimatorNoAds;
    public Animator AnimatorMapButton;

    public GameObject TopDistancePrefab;
    private GameObject TopDistanceObject;
    public IntialAndBanner theBanner;
    public GameObject theTapScreen;
    [SerializeField] private SimpleScrollSnap TheMapPage;
    public Animator HoldTapAnimator;
    private bool isNoAds;
    [SerializeField] private GameObject developerScreen;
    public AudioSource buttonClickSound;

    void Start()
    {
        isNoAds = false;
        if (PlayerPrefs.GetInt("isNoAds") == 1)
        {
            isNoAds = true;
        }
        isFPS30 = false;
        isStartGame = false;
        thePlayer = FindObjectOfType<PlayerController>();
        theCamera = FindObjectOfType<CameraController>();
        theUICoin = GetComponent<CreateUICoin>();
        isCoinCount = false;
        isAdsClicked = false;
        UpdateLevelCount();
        isShopOpen = false;
        shopScreenPage = 0;
        mapScreenPage = 2;
        MarkTopDistance();
        currentSceneName = SceneManager.GetActiveScene().name;
        PlayerPrefs.SetString("LastSceneName", currentSceneName);
        cameraCount = PlayerPrefs.GetInt("CameraSetting");
        ToggleCamera();
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump") && !isStartGame)
        {
            StartCoroutine(StartGame());
        }

        if (isCoinCount)
        {
            if (moveCoinTochangeSceneCounter <= 0)
            {
                isCoinCount = false;
                ChangeScene();
            }
        }
    }

    #region  Screen
    public void ClickToStartGame()
    {
        if (!isStartGame)
            StartCoroutine(StartGame());
    }

    IEnumerator StartGame()
    {
        thePlayer.TurningFront();
        StartText.CrossFadeAlpha(0.0f, fadeOutTime, false);
        yield return new WaitForSeconds(fadeOutTime);
        if (HoldTapAnimator)
            HoldTapAnimator.SetTrigger("HoldTap");
        isStartGame = true;
        CloseShopScreen();
        HideButton();
        theCamera.enabled = true;
        theCamera.isStartGame = true;
        isShopOpen = false;

        yield return new WaitForSeconds(1.0f);
        shopButtonObject.SetActive(false);
        mapButtonObject.SetActive(false);
        noAdsButtonObject.SetActive(false);
        StartTextObject.SetActive(false);

    }

    private void MarkTopDistance()
    {
        float topDistance = PlayerPrefs.GetFloat("TopDistance");
        if ((int)topDistance > 0)
        {
            DestroyTopDistance();
            //TopDistanceObject = Instantiate(TopDistancePrefab, new Vector3(0, 0, thePlayer.realTopDistance), Quaternion.Euler(new Vector3(0, 0, 0)));
            //TopDistanceObject.GetComponentInChildren<TMPro.TextMeshPro>().text = (int)topDistance + "m";
        }
    }

    private void DestroyTopDistance()
    {
        if (TopDistanceObject)
            Destroy(TopDistanceObject);
    }

    public IEnumerator StartNewScreen()
    {
        isStartGame = false;
        MarkTopDistance();
        shopButtonObject.SetActive(true);
        mapButtonObject.SetActive(true);
        noAdsButtonObject.SetActive(true);
        StartTextObject.SetActive(true);
        StartText.CrossFadeAlpha(1.0f, fadeOutTime, false);
        ShowButton();
        yield return new WaitForSeconds(fadeOutTime);
    }

    public void GameOver()
    {
        gameOverScreen.SetActive(true);
        Over.canvasRenderer.SetAlpha(0.0f);
        Over.CrossFadeAlpha(1.0f, fadeOutTime, false);
    }

    public IEnumerator Restart()
    {
        isCoinCount = false;
        isAdsClicked = false;
        FinishText.CrossFadeAlpha(0.0f, fadeOutTime, false);
        ReviveButtonObject.SetActive(false);
        NoThanksObject.SetActive(false);
        X1ButtonObject.SetActive(false);
        X2ButtonObject.SetActive(false);
        X3ButtonObject.SetActive(false);
        X4ButtonObject.SetActive(false);
        stageCoinObject1.SetActive(false);
        yield return new WaitForSeconds(fadeOutTime);
        goalScreen.SetActive(false);
    }

    public IEnumerator FinishEndless(string Scene)
    {
        levelToChange = Scene;
        NoThanksObject.SetActive(false);
        goalScreen.SetActive(true);
        FinishText.canvasRenderer.SetAlpha(0.0f);
        FinishText.CrossFadeAlpha(1.0f, fadeOutTime, false);
        int randomNumber = Random.Range(1, 16);
        int randomNumberRevive = Random.Range(0, 2);
        yield return new WaitForSeconds(1f);
        if (thePlayer.cheatReviveCount > 2)
        {
            ReviveButtonObject.SetActive(true);
            ReviveButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
            ReviveText.canvasRenderer.SetAlpha(0.0f);
            ReviveButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
            ReviveText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            yield return new WaitForSeconds(2.0f);
            if (!isAdsClicked)
            {
                NoThanksObject.SetActive(true);
                NoThanksText.canvasRenderer.SetAlpha(0.0f);
                NoThanksText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
        }
        else if (!thePlayer.isRevived && thePlayer.jumpDistance > 50)
        {
            if (randomNumberRevive == 1)
            {
                ReviveButtonObject.SetActive(true);
                ReviveButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                ReviveText.canvasRenderer.SetAlpha(0.0f);
                ReviveButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                ReviveText.CrossFadeAlpha(1.0f, fadeOutTime, false);
                yield return new WaitForSeconds(2.0f);
                if (!isAdsClicked)
                {
                    NoThanksObject.SetActive(true);
                    NoThanksText.canvasRenderer.SetAlpha(0.0f);
                    NoThanksText.CrossFadeAlpha(1.0f, fadeOutTime, false);
                }
            }
            else
            {
                Respawn();
            }
        }
        else if (thePlayer.stageCoin > 5 && randomNumber > 5 && randomNumberRevive == 0)
        {
            if (randomNumber <= 9)
            {
                X1ButtonObject.SetActive(true);
                X1ButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                X1ButtonText.canvasRenderer.SetAlpha(0.0f);
                X1ButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                X1ButtonText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
            else if (randomNumber <= 12)
            {
                X2ButtonObject.SetActive(true);
                X2ButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                X2ButtonText.canvasRenderer.SetAlpha(0.0f);
                X2ButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                X2ButtonText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
            else if (randomNumber <= 14)
            {
                X3ButtonObject.SetActive(true);
                X3ButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                X3ButtonText.canvasRenderer.SetAlpha(0.0f);
                X3ButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                X3ButtonText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
            else if (randomNumber == 15)
            {
                X4ButtonObject.SetActive(true);
                X4ButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                X4ButtonText.canvasRenderer.SetAlpha(0.0f);
                X4ButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                X4ButtonText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
            stageCoinObject1.SetActive(true);
            stageCoin1.text = thePlayer.stageCoin + " Fotu";
            stageCoin1.canvasRenderer.SetAlpha(0.0f);
            stageCoin1.CrossFadeAlpha(1.0f, fadeOutTime, false);
            yield return new WaitForSeconds(2.0f);
            if (!isAdsClicked)
            {
                NoThanksObject.SetActive(true);
                NoThanksText.canvasRenderer.SetAlpha(0.0f);
                NoThanksText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
        }
        else
        {
            Respawn();
        }
    }

    public IEnumerator FinishLevel(string Scene)
    {
        levelToChange = Scene;
        NoThanksObject.SetActive(false);
        goalScreen.SetActive(true);
        DeathText.canvasRenderer.SetAlpha(0.0f);
        FinishText.canvasRenderer.SetAlpha(0.0f);
        FinishText.CrossFadeAlpha(1.0f, fadeOutTime, false);
        int randomNumber = Random.Range(1, 16);
        int randomNumberRevive = Random.Range(0, 2);
        yield return new WaitForSeconds(1f);
        if (thePlayer.cheatReviveCount > 2)
        {
            ReviveButtonObject.SetActive(true);
            ReviveButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
            ReviveText.canvasRenderer.SetAlpha(0.0f);
            ReviveButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
            ReviveText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            yield return new WaitForSeconds(2.0f);
            if (!isAdsClicked)
            {
                NoThanksObject.SetActive(true);
                NoThanksText.canvasRenderer.SetAlpha(0.0f);
                NoThanksText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
        }
        else if (!thePlayer.isRevived && thePlayer.jumpDistance > 50)
        {
            if (randomNumberRevive == 1)
            {
                ReviveButtonObject.SetActive(true);
                ReviveButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                ReviveText.canvasRenderer.SetAlpha(0.0f);
                ReviveButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                ReviveText.CrossFadeAlpha(1.0f, fadeOutTime, false);
                yield return new WaitForSeconds(2.0f);
                if (!isAdsClicked)
                {
                    NoThanksObject.SetActive(true);
                    NoThanksText.canvasRenderer.SetAlpha(0.0f);
                    NoThanksText.CrossFadeAlpha(1.0f, fadeOutTime, false);
                }
            }
            else
            {
                ChangeScene();
            }
        }
        else if (thePlayer.stageCoin > 5 && randomNumber > 5 && randomNumberRevive == 0)
        {
            if (randomNumber <= 9)
            {
                X1ButtonObject.SetActive(true);
                X1ButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                X1ButtonText.canvasRenderer.SetAlpha(0.0f);
                X1ButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                X1ButtonText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
            else if (randomNumber <= 12)
            {
                X2ButtonObject.SetActive(true);
                X2ButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                X2ButtonText.canvasRenderer.SetAlpha(0.0f);
                X2ButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                X2ButtonText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
            else if (randomNumber <= 14)
            {
                X3ButtonObject.SetActive(true);
                X3ButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                X3ButtonText.canvasRenderer.SetAlpha(0.0f);
                X3ButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                X3ButtonText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
            else if (randomNumber == 15)
            {
                X4ButtonObject.SetActive(true);
                X4ButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                X4ButtonText.canvasRenderer.SetAlpha(0.0f);
                X4ButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                X4ButtonText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
            stageCoinObject1.SetActive(true);
            stageCoin1.text = thePlayer.stageCoin + " Fotu";
            stageCoin1.canvasRenderer.SetAlpha(0.0f);
            stageCoin1.CrossFadeAlpha(1.0f, fadeOutTime, false);
            yield return new WaitForSeconds(2.0f);
            if (!isAdsClicked)
            {
                NoThanksObject.SetActive(true);
                NoThanksText.canvasRenderer.SetAlpha(0.0f);
                NoThanksText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
        }
        else
        {
            ChangeScene();
        }
    }

    public IEnumerator DeathLevel()
    {
        NoThanksObject.SetActive(false);
        goalScreen.SetActive(true);
        FinishText.canvasRenderer.SetAlpha(0.0f);
        DeathText.canvasRenderer.SetAlpha(0.0f);
        DeathText.CrossFadeAlpha(1.0f, fadeOutTime, false);
        int randomNumber = Random.Range(1, 16);
        int randomNumberRevive = Random.Range(0, 2);
        yield return new WaitForSeconds(1f);
        if (thePlayer.cheatReviveCount > 2)
        {
            ReviveButtonObject.SetActive(true);
            ReviveButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
            ReviveText.canvasRenderer.SetAlpha(0.0f);
            ReviveButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
            ReviveText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            yield return new WaitForSeconds(2.0f);
            if (!isAdsClicked)
            {
                NoThanksObject.SetActive(true);
                NoThanksText.canvasRenderer.SetAlpha(0.0f);
                NoThanksText.CrossFadeAlpha(1.0f, fadeOutTime, false);
            }
        }
        else if (!thePlayer.isRevived && thePlayer.jumpCounter > 5)
        {
            if (randomNumberRevive == 1)
            {
                ReviveButtonObject.SetActive(true);
                ReviveButtonObject.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
                ReviveText.canvasRenderer.SetAlpha(0.0f);
                ReviveButtonObject.GetComponent<Image>().CrossFadeAlpha(1.0f, fadeOutTime, false);
                ReviveText.CrossFadeAlpha(1.0f, fadeOutTime, false);
                yield return new WaitForSeconds(2.0f);
                if (!isAdsClicked)
                {
                    NoThanksObject.SetActive(true);
                    NoThanksText.canvasRenderer.SetAlpha(0.0f);
                    NoThanksText.CrossFadeAlpha(1.0f, fadeOutTime, false);
                }
            }
            else
            {
                Respawn();
            }
        }
        else
        {
            Respawn();
        }
    }

    public void Pause()
    {
        buttonClickSound.Play();
        Time.timeScale = 0;
        PauseScreen.SetActive(true);
    }

    public void Resume()
    {
        buttonClickSound.Play();
        Time.timeScale = 1;
        PauseScreen.SetActive(false);
    }

    public void MainMenu()
    {
        buttonClickSound.Play();
        Time.timeScale = 1;
        PauseScreen.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ResetGame()
    {
        buttonClickSound.Play();
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    #endregion

    #region Count Screen
    public void UpdateJumpCount(int jumpCoun)
    {
        jumpCounter.text = jumpCoun + "m";
    }

    public void UpdateJumpGroundCount(int jumpCoun)
    {
        jumpGroundCount.text = jumpCoun + "";
    }

    public void UpdateTopDistance(int Dis)
    {
        TopDistanceText.text = Dis + "m";
    }

    public void UpdateCoinCount(int coinCoun)
    {
        coinCounter.text = coinCoun.ToString();
    }

    public void RemoveCoin(int coinToRemove)
    {
        thePlayer.coinCounter -= coinToRemove;
        int newCoin = PlayerPrefs.GetInt("Coin") - coinToRemove;
        thePlayer.startCoin = newCoin;
        PlayerPrefs.SetInt("Coin", newCoin);
        UpdateCoinCount(newCoin);
    }

    public void AddCoinUI(int coinToAdd)
    {
        int newCoin = thePlayer.coinCounter += coinToAdd;
        AnimeAddingCoinUI.Play("UIAddCoin");
        UpdateCoinCount(newCoin);
    }

    public void UpdateLevelCount()
    {
        string currentMap = SceneManager.GetActiveScene().name;
        string currentMapName = currentMap.Split(' ')[0];
        string currentMapNumber = currentMap.Split(' ')[1];
        if (currentMapName.Equals("OLevel"))
        {
            levelCounter.text = "Ocean " + currentMapNumber;
            levelCounter.color = new Color(85f / 255f, 197f / 255f, 255f / 255f);
        }
        else if (currentMapName.Equals("HLevel"))
        {
            levelCounter.text = "Hill " + currentMapNumber;
            levelCounter.color = new Color(245f / 255f, 164f / 255f, 82f / 255f);
        }
        else
        {
            levelCounter.text = "Snow " + currentMapNumber;
            levelCounter.color = Color.white;
        }
    }
    #endregion

    public void OpenMapScreen()
    {
        if (!isShopOpen && !isStartGame)
        {
            buttonClickSound.Play();
            isMapScreenOpen = true;
            theMapOpenAnimator.SetBool("isMapScreenOpen", true);
            theMapScreen.SetActive(true);
            theTapScreen.SetActive(false);
            HideButton();
            // if (TheMapPage)
            //     TheMapPage.GoToPanel(mapScreenPage);
        }
    }

    public void CloseMapScreen()
    {
        // if (TheMapPage)
        //     mapScreenPage = TheMapPage.CurrentPanel;
        // Debug.Log(TheMapPage.CurrentPanel);
        buttonClickSound.Play();
        isMapScreenOpen = false;
        theMapOpenAnimator.SetBool("isMapScreenOpen", false);
        theMapScreen.SetActive(false);
        theTapScreen.SetActive(true);
        ShowButton();
    }

    public void OpenShopScreen()
    {
        if (!isMapScreenOpen && !isStartGame)
        {
            buttonClickSound.Play();
            isShopOpen = true;
            theShopScreen.SetActive(true);
            theTapScreen.SetActive(false);
            HideButton();
        }
    }

    public void CloseShopScreen()
    {
        buttonClickSound.Play();
        ShowButton();
        isShopOpen = false;
        theShopScreen.SetActive(false);
        theTapScreen.SetActive(true);
    }

    private void HideButton()
    {
        isShopOpen = true;
        AnimatorMapButton.SetBool("isShopOpen", isShopOpen);
        AnimatorShopButton.SetBool("isShopOpen", isShopOpen);
        AnimatorNoAds.SetBool("isShopOpen", isShopOpen);
        if (Screen1Animator)
            Screen1Animator.SetBool("isShopOpen", isShopOpen);
    }

    private void ShowButton()
    {
        isShopOpen = false;
        AnimatorMapButton.SetBool("isShopOpen", isShopOpen);
        AnimatorShopButton.SetBool("isShopOpen", isShopOpen);
        AnimatorNoAds.SetBool("isShopOpen", isShopOpen);
        if (Screen1Animator)
            Screen1Animator.SetBool("isShopOpen", isShopOpen);
    }

    #region Ads
    public void NoAds()
    {
        buttonClickSound.Play();
        theBanner.HideBanner();
        PlayerPrefs.SetInt("isNoAds", 1);
    }

    public void X1()
    {
        buttonClickSound.Play();
        X1ButtonObject.SetActive(false);
        PlayerPrefs.SetInt("Coin", thePlayer.coinCounter + thePlayer.stageCoin * 1);
        StartCoroutine(XCoinAnimation(thePlayer.stageCoin * 1));
    }

    public void X2()
    {
        buttonClickSound.Play();
        X2ButtonObject.SetActive(false);
        PlayerPrefs.SetInt("Coin", thePlayer.coinCounter + thePlayer.stageCoin * 2);
        StartCoroutine(XCoinAnimation(thePlayer.stageCoin * 2));
    }

    public void X3()
    {
        buttonClickSound.Play();
        X3ButtonObject.SetActive(false);
        PlayerPrefs.SetInt("Coin", thePlayer.coinCounter + thePlayer.stageCoin * 3);
        StartCoroutine(XCoinAnimation(thePlayer.stageCoin * 3));
    }

    public void X4()
    {
        buttonClickSound.Play();
        X4ButtonObject.SetActive(false);
        PlayerPrefs.SetInt("Coin", thePlayer.coinCounter + thePlayer.stageCoin * 4);
        StartCoroutine(XCoinAnimation(thePlayer.stageCoin * 4));
    }

    public void Plus100()
    {
        buttonClickSound.Play();
        PlayerPrefs.SetInt("Coin", thePlayer.coinCounter + 100);
        StartCoroutine(PlusCoinAnimation(100));
    }

    public void NoThanks()
    {
        buttonClickSound.Play();
        if (thePlayer.isGoal)
            ChangeScene();
        else
            Respawn();
    }

    public IEnumerator XCoinAnimation(int newStageCoin)
    {
        isAdsClicked = true;
        NoThanksObject.SetActive(false);
        moveCoinTochangeSceneCounter = newStageCoin;
        isCoinCount = true;
        if (thePlayer.stageCoin != newStageCoin)
        {
            for (int i = thePlayer.stageCoin; i <= newStageCoin; i++)
            {

                stageCoin1.text = i + " Tofu";
                yield return new WaitForSeconds(0.01f);
            }
            yield return new WaitForSeconds(0.5f);
        }
        for (int j = newStageCoin; j >= 0; j--)
        {
            if (j != 0)
                theUICoin.CreateMoveCoin();
            stageCoin1.text = j + " Tofu";
            yield return new WaitForSeconds(0.01f);
        }
        yield return new WaitForSeconds(5.0f);
    }

    public IEnumerator PlusCoinAnimation(int newStageCoin)
    {
        PlayerPrefs.SetInt("Coin", thePlayer.coinCounter + 100);
        isAdsClicked = true;
        NoThanksObject.SetActive(false);
        moveCoinTochangeSceneCounter = newStageCoin;
        for (int j = newStageCoin; j >= 0; j--)
        {
            if (j != 0)
                theUICoin.CreateMoveCoinPlus();
            yield return new WaitForSeconds(0.01f);
        }
        theShop = FindObjectOfType<ShopController>();
        if (theShop)
            theShop.UpdateShop();
    }

    public void RemoveCoinTochangeSceneCounter()
    {
        moveCoinTochangeSceneCounter -= 1;
    }
    #endregion

    public void ChangeScene()
    {
        FinishTextedLevelCount++;
        if (FinishTextedLevelCount >= 10 && !isNoAds)
        {
            GetComponent<UnityAdsPlacement>().ShowAd();
            FinishTextedLevelCount = 0;
        }
        SceneManager.LoadScene(levelToChange);
    }

    public void ChangeMap(string name)
    {
        if (!SceneManager.GetActiveScene().Equals(name))
            SceneManager.LoadScene(name);
    }

    private void Respawn()
    {
        FinishTextedLevelCount++;
        if (FinishTextedLevelCount >= 20 && !isNoAds)
        {
            GetComponent<UnityAdsPlacement>().ShowAd();
            FinishTextedLevelCount = 0;
        }
        StartCoroutine(thePlayer.Respawn());
    }

    public void ResetObject()
    {
        for (int i = 0; i < objectsToReset.Length; i++)
        {
            objectsToReset[i].ResetObject();
            objectsToReset[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < objectsToResetParent.Length; i++)
        {
            foreach (Transform child in objectsToResetParent[i].transform)
            {
                child.GetComponent<ResetOnRespawn>().ResetObject();
                child.gameObject.SetActive(true);
            }
        }
    }

    public void ChangeFPS()
    {
        buttonClickSound.Play();
        isFPS30 = !isFPS30;
        if (isFPS30)
            Application.targetFrameRate = 30;
        else
            Application.targetFrameRate = 60;
    }

    public void ToggleCamera()
    {
        buttonClickSound.Play();
        Camera1.SetActive(false);
        Camera2.SetActive(false);
        Camera3.SetActive(false);
        Camera4.SetActive(false);
        if (cameraCount > 4)
        {
            cameraCount = 1;
        }
        else if (cameraCount <= 0)
        {
            cameraCount = 4;
        }

        if (cameraCount == 1)
        {
            Camera1.SetActive(true);
            Camera1.GetComponent<CameraController>().isStartGame = true;
        }
        else if (cameraCount == 2)
        {
            Camera2.SetActive(true);
            Camera2.GetComponent<CameraController>().isStartGame = true;
        }
        else if (cameraCount == 3)
        {
            Camera3.SetActive(true);
            Camera3.GetComponent<CameraController>().isStartGame = true;
        }
        else if (cameraCount == 4)
        {
            Camera4.SetActive(true);
            Camera4.GetComponent<CameraController>().isStartGame = true;
        }
        PlayerPrefs.SetInt("CameraSetting", cameraCount);
        cameraCount++;
    }

    public void OpenDeveloperScreen()
    {
        buttonClickSound.Play();
        developerScreen.SetActive(true);
    }

    public void CloseDeveloperScreen()
    {
        developerScreen.SetActive(false);
    }
}
