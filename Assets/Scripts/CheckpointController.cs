﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointController : MonoBehaviour
{
    public bool isCheckpointActivated;
    public GameObject CheckpointActivated;
    public GameObject CheckpointDeactivated;

    void Start()
    {
        isCheckpointActivated = false;
    }

    public void ActivateCheckpoint()
    {
        CheckpointDeactivated.SetActive(false);
        CheckpointActivated.SetActive(true);
    }
}
