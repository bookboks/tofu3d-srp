﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUICoin : MonoBehaviour
{
    private LevelManager theLevelManager;
    public GameObject Target;
    public GameObject MoveCoin;
    float step;
    float step2;
    private float RandomMoveCounter;
    private float RandomMoveTime = 0.5f;
    private Vector3 RandomTarget;
    private float startTime = 0;

    void Start()
    {
        theLevelManager = FindObjectOfType<LevelManager>();
        step = 2000.0f * Time.deltaTime;
        step2 = 2000.0f * Time.deltaTime;
        Target = GameObject.Find("TargetCoin");
        RandomMoveCounter = RandomMoveTime;
        RandomTarget = RandomVector(transform.position.x, transform.position.y, transform.position.z);
    }

    void Update()
    {
        startTime += Time.deltaTime;
        if (RandomMoveCounter > 0)
        {
            RandomMoveCounter -= Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, RandomTarget, step);
        }
        else
            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, step2);

        if (transform.position == Target.transform.position)
        {
            theLevelManager.AddCoinUI(1);
            theLevelManager.RemoveCoinTochangeSceneCounter();
            Destroy(gameObject);
        }
    }

    private Vector3 RandomVector(float posX, float posY, float posZ)
    {
        float k = 150.0f;
        var x = Random.Range(posX - k, posX + k);
        var y = Random.Range(posY - k, posY + k);
        var z = Random.Range(posZ - k, posZ + k);
        return new Vector3(x, y, z);
    }
}
