﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemController : MonoBehaviour
{
    private ParticleSystem Paticle;
    private float particleCounter;
    private float particleLifeTime = 0.5f;

    void Start()
    {
        Paticle = GetComponent<ParticleSystem>();
        particleCounter = particleLifeTime;
    }

    void Update()
    {
        if (Paticle)
        {
            if (particleCounter <= 0)
            {
                Destroy(gameObject);
            }
            else
            {
                particleCounter -= Time.deltaTime; ;
            }
        }
    }
}
