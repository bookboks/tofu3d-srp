﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour
{
    public bool isCubeActived;
    [SerializeField]
    private Animator cubeAnime;
    [SerializeField]
    private GameObject FadeCube;
    public string cubeName = "normalCube";
    void Start()
    {
        isCubeActived = false;
    }

    void Update()
    {

    }

    public void ActiveCube()
    {
        isCubeActived = true;
        if (cubeAnime)
            cubeAnime.SetBool("isActive", isCubeActived);
        //Instantiate(FadeCube, new Vector3(transform.position.x, transform.position.y + 0.79f, transform.position.z), Quaternion.Euler(new Vector3(0, 0, 0)));
    }
}
